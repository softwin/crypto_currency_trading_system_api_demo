封装说明：
基类BaseStrategy实现了一系列api，主要分为以下几类：
1. 报撤单函数，如insertOrder, buy, open, cancelOrder，和对应的期货操作
2. 远程查询函数，queryXXX，用于向服务器查询账号，资金，委托，成交，币对，合约，仓位等
3. 本地查询函数，getXXXX，策略启动初始化时自动向服务器查询资金和未完成的委托，此后，自动根据报撤单动作维护上述相关信息，以便更加快捷的查询
4. 行情订阅函数，sub/unsub XXXX，用于定于期货和现货行情


StrategySpi定义了一些列spi，用于实现回调函数，主要分以下几类：
1. 策略启停，如onInit，onStop函数，在策略启动和停止时会被回调
2. 服务器状态，onConnectionStatusChange，当服务器连上或断开时被回调。注：BaseStrategy已经包含了断线重连时重新订阅行情和回报的逻辑。
3. 行情回调，如onTick等，在行情数据到来时会被回调
4. 交易回调，如onRtnTrade, onRtnOrder，为委托和成交的回调

编写策略
1. 继承BaseStrategy以便调用相关api
2. 实现StrategySpi中策略关心的回调接口
3. 调用start方法启动策略

