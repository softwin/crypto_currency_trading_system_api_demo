package com.softwin.cryptocurrency.demo;

import com.softwin.marketdata.gateway.service.MarketGateway;

public class StrategyConstants {

    public static final String FIRST_EXCHANGE = "huobi";
    public static final String SECOND_EXCHANGE = "okex";
    public static final String BASE_CURRENCY = "btc";
    public static final String QUOTE_CURRENCY = "usdt";
    public static final int POLLING_PERIOD = 3;
    public static final MarketGateway.KlinePeriod KLINE_PERIOD  = MarketGateway.KlinePeriod.ONE_MINUTE;

}
