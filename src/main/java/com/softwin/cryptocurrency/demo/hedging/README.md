#策略逻辑说明
本例是一个基于市场深度的跨市套利策略，旨在示范如何使用虚拟货币资管系统的API，并不具备实际盈利价值，甚至会为了更好的演示效果而在不利条件下进行交易。
本例的交易标的物是两个交易所之间的单一合约(火币网&币安 btcusdt币对)
## 主要的交易逻辑
1. 分别在交易所A与交易所B 持有btc 和 usdt
2. 订阅两个交易所币对的市场深度行情，收取币对最近的买单报价和卖单报价信息
3. 如果A交易所的最高买单报价（max bids） > B交易所的最低卖单报价（min asks），且他们的差超过预设的阈值minSpreadA，则存在套利空间，可以在A交易所用卖出btc得到usdt，并在B交易所用等量usdt买入btc；
    反之类似，
4.如果存在套利机会，则计算下单价格(operationPrice)和操作数量(operationNum)，报单价格取最大买单报价和最低卖单报价的中间值，即（max bids + min asks）/2，
操作数量取（预设的最大交易量，最高买单量，最低卖单量，用户账号btc量，用户usdt/下单价格） 这五者的最小值。 
 并分别在提供最高买单的交易所挂卖单，在提供最低卖单的交易所挂买单，数量价格按刚刚计算的设定。

# 服务接口基本情况
## 行情网关，8991端口，有一个grpc服务:
1. 行情服务，MarketDataManage，不需登录，参见marketGateway.proto及相关文档
## 订单处理网关，8993端口，有三个grpc服务：
1. 登录服务，Login，不需登录，参见login.proto
2. 报单服务，OrderReporting，需登录，参见orderreporting.proto及相关文档
3. 查询服务，QueryService，需登录，参见orderreporting.proto及相关文档

# 策略代码说明
## TwoExchangeHedging类中的checkBuySellPoint方法为策略主要逻辑
1. 首先初始化各个grpc服务
2. 登录订单处理网关，提交用户名密码，获得token
3. 订阅成交回报
4. 订阅depth行情
5. 根据两个交易所bids asks情况进行报单
6. 根据成交回报维护仓位信息

## JwtCallCredential类为请求添加身份标识token
1. 拦截器，在调用需要身份验证的grpc接口时在header中填入token